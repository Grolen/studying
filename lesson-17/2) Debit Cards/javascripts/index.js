/**
 * Создать функции для редактирования превью кард
 * И для сохранения карты в кошельке
 * cartPreviewСhange должен уметь валедировать
 * card_number что бы только числа были а также cvv.
 * Впревью карте точки должны заменяться на числа.
 * Месяц и год тоже должны изменяться при изменении в селектор.
 *
 * Функция addPaymentCart Должна брать значения из инпутов и селектов. и создавать новый способ оплаты в кошельке
 * А также проверять номер карты к какому типу карт она относится мастер кард или виза.
 * */

const CARD_TYPE_VISA = 'visa';
const CARD_TYPE_MASTERCARD = 'mastercard';
const CARD_TYPE_MAESTRO = 'maestro';
const CARD_TYPE_AMEX = 'amex';
const CARDS_CONFIG = {
  [CARD_TYPE_VISA]: {
    regex: '^4',
    count: 16,
  },
  [CARD_TYPE_MASTERCARD]: {
    regex: '^(5[1-5]|(222[1-8][0-9]{2}|2229[0-8][0-9]|22299[0-9]|22[3-9][0-9]{3}|2[3-6][0-9]{4}|27[01][0-9]{3}|2720[0-8][0-9]|27209[0-9]))',
    count: 16,
  },
  [CARD_TYPE_MAESTRO]: {
    regex: '^(5018|5020|5038|6304|6759|6761|6762|6763)',
    count: 19,
  },
  [CARD_TYPE_AMEX]: {
    regex: '^(34|37)',
    count: 15,
  },
};

const getCardTypeByNumber = (cardNumber = '') => {
  for (const type of Object.keys(CARDS_CONFIG)) {
    if (cardNumber.match(new RegExp(CARDS_CONFIG[type].regex))) {
      return type;
    }
  }

  return '';
};
const getCardCount = (cardType) => (CARDS_CONFIG[cardType] ? CARDS_CONFIG[cardType].count : '');

function validate(evt) {
  let theEvent = evt || window.event;
  let key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode(key);
  const regex = /[0-9]|\./;
  if (!regex.test(key)) {
    theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
  }
}

function cartPreviewСhange() {
  const inputCardNumber = document.getElementById('card_number');
  inputCardNumber.addEventListener('input', () => {
    const cardNumber = inputCardNumber.value;
    const maxLength = getCardTypeByNumber(cardNumber) ? getCardCount(getCardTypeByNumber(cardNumber)) : 16;
    const mask = [];
    inputCardNumber.maxLength = maxLength;
    for (let i = 0; i < maxLength; i++) {
      mask.push(`<span>${cardNumber[i] || '•'}</span>`);
    }
    const cardNumberPreview = document.querySelector('.card_number');
    cardNumberPreview.innerHTML = mask.toString().replace(/,/g, ' ');
    const cardTypePreview = document.querySelector('.card_logo');
    cardTypePreview.className = `card__logo card_logo ${getCardTypeByNumber(cardNumber)}`;
  });
  const formChange = document.querySelector('.card_form');
  formChange.addEventListener('change', (event) => {
    if (event.target.name === 'card_month') {
      const monthPreview = document.querySelector('.expiry_month');
      for (const option of event.target) {
        if (option.selected) {
          monthPreview.innerText = option.value;
        }
      }
    }
    if (event.target.name === 'card_year') {
      const yearPreview = document.querySelector('.expiry_year');
      for (const option of event.target) {
        if (option.selected) {
          yearPreview.innerText = option.value;
        }
      }
    }
    if (event.target.name === 'card_cvv') {
      if (isNaN(event.target.value) === true) {
        event.target.value = '';
        event.target.placeholder = 'Incorrect value';
      }
    }
  });
}

function addPaymentCart() {
  const cardWallet = document.querySelector('.wallet_wrap');
  const button = document.querySelector('button');
  const month = document.querySelector('.expiry_month');
  const year = document.querySelector('.expiry_year');
  button.addEventListener('click', () => {
    const cardNumberPreview = document.querySelectorAll('.card_number>span');
    let arrSpan = [];
    for (let i = 0; i < cardNumberPreview.length; i++) {
      arrSpan.push(`<span>${cardNumberPreview[i].innerText}</span>`);
    }
    if (arrSpan.length === 16 || arrSpan.length === 15) {
      for (let i = 0; i < 12; i++) {
        arrSpan[i] = `<span>*</span>`;
      }
    } else if (arrSpan.length === 19) {
      for (let i = 0; i < 16; i++) {
        arrSpan[i] = `<span>*</span>`;
      }
    }
    const inputCardNumber = document.getElementById('card_number');
    const cardNumber = inputCardNumber.value;
    const cardType = getCardTypeByNumber(cardNumber);
    if (cardType !== '' && cardNumber.length === arrSpan.length && month.innerText !== '00' && year.innerText !== '00') {
      cardWallet.insertAdjacentHTML(
        'beforeend',
        `<div class="card-wallet">
      <div class="wallet__container">
        <div class="wallet__side-icon _${getCardTypeByNumber(cardNumber)}"></div>
        <div class="wallet__content">
          <div class="wallet__title">
          ${arrSpan.toString().replace(/,/g, '')}
          </div>
          <div class="wallet__desc">Exp: ${month.innerText}/${year.innerText}</div>
        </div>
      </div>
    </div>`
      );
    }
  });
}

cartPreviewСhange();
addPaymentCart();
