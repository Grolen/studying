import React, { Component } from 'react';
import Button from './components/Button/Button';
import ProductList from './components/ProductList/ProductList';
import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';

const getHistoryFromLS = (itemArr) => {
  const lsHistory = localStorage.getItem(itemArr);
  if (!lsHistory) {
    return [];
  }
  try {
    const value = JSON.parse(lsHistory);
    return value;
  } catch (error) {
    return [];
  }
};

export default class App extends Component {
  constructor(props) {
    super(props);
    const lsFavorite = getHistoryFromLS('favorite');
    const lsCart = getHistoryFromLS('cart');
    this.state = {
      products: [],
      loading: true,
      favorite: lsFavorite,
      cart: lsCart,
      modalShow: false,
      modalContent: {
        header: 'Modal window',
        text: 'Do you want add this item to cart?',
        closeButton: true,
        actions: '',
      },
    };
  }

  async componentDidMount(prevProps, prevState) {
    const response = await fetch('googlePhones.json');
    const data = await response.json();
    this.setState({
      products: data.phones,
      loading: false,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { favorite, cart } = this.state; // проверяем наличие в локальном хранилище изменений в избранном
    if (prevState.favorite !== favorite) {
      localStorage.setItem('favorite', JSON.stringify(favorite));
    }
    if (prevState.cart !== cart) {
      localStorage.setItem('cart', JSON.stringify(cart));
    }
  }

  //For Button "Add to cart"
  showModal = () => {
    this.setState({ modalShow: true });
  };

  generateModal = (productID, nameOfProduct) => {
    this.showModal();
    this.setState({
      modalContent: {
        ...this.state.modalContent,
        text: `Add ${nameOfProduct} to cart?`,
        actions: (
          <>
            <button type="button" className="modal__content__btn" onClick={(event) => this.addItemToCart(productID)}>
              Yes
            </button>
            <button type="button" className="modal__content__btn" onClick={this.closeModal}>
              No
            </button>
          </>
        ),
      },
    });
  };

  closeModal = () => {
    this.setState({ modalShow: false });
  };

  addItemToCart = (productId) => {
    this.closeModal();
    const product = this.state.products.find((product) => product.articul === productId);
    this.setState({
      cart: [...this.state.cart, product],
    });
  };

  //For Span "Favorite"
  addFavoriteProduct = (productId) => {
    console.log(productId);
    const product = this.state.products.find((product) => product.articul === productId);
    this.setState({
      favorite: [...this.state.favorite, product],
    });
  };

  deleteFavoriteProduct = (productId) => {
    this.setState({
      favorite: this.state.favorite.filter((product) => product.articul !== productId),
    });
  };

  //For Header
  productsInCartCounter = () => {
    const number = this.state.cart.length;
    return number;
  };

  productsInFavoriteCounter = () => {
    const number = this.state.favorite.length;
    return number;
  };

  render() {
    return (
      <>
        <Header productsInCart={this.productsInCartCounter()} productsInFavorite={this.productsInFavoriteCounter()} />
        <ProductList
          loading={this.state.loading}
          products={this.state.products}
          onClickBtn={this.generateModal}
          favoriteProduct={this.addFavoriteProduct}
          unfavoriteProduct={this.deleteFavoriteProduct}
        />
        {/* <Button modalID="modalID1" title="Open first modal" onClick={this.generateModal} backGround="red" />
        <Button modalID="modalID2" title="Open second modal" onClick={this.generateModal} backGround="blue" /> */}
        {this.state.modalShow && <Modal modalContent={this.state.modalContent} onClose={this.closeModal} />}
      </>
    );
  }
}
