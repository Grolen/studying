import React, { Component } from 'react';
import Button from '../Button/Button';
import { ReactComponent as StarSvg } from '../../img/star.svg';

export default class ProductCard extends Component {
  constructor() {
    super();
    this.state = {
      isFavorite: false,
    };
  }

  toggleFavorite = (productId) => {
    this.setState({
      isFavorite: !this.state.isFavorite,
    });
    if (!this.state.isFavorite) {
      this.props.favorite(productId);
    } else {
      this.props.unfavorite(productId);
    }
  };

  render() {
    const { phone, onClickBtn } = this.props;
    return (
      <div key={phone.articul} id={phone.articul} className="item">
        <div className="item__image">
          <img src={phone.image} alt={phone.name} width="350px" height="350px" />
        </div>
        <div className="item__info">
          <h2>
            {phone.name} ({phone.color})
          </h2>
          <span onClick={() => this.toggleFavorite(phone.articul)}>{this.state.isFavorite ? <StarSvg fill="green" /> : <StarSvg />}</span>

          <p>{phone.price}</p>
        </div>
        <div className="item__buttons">
          <Button productID={phone.articul} productFullName={`${phone.name} (${phone.color})`} title="Add to cart" backGround="red" onClick={onClickBtn} />
        </div>
      </div>
    );
  }
}
