import React, { Component } from 'react';
import './Button.scss';

export default class Button extends Component {
  clickHeandler = (e) => {
    const productID = e.target.dataset.productId;
    this.props.onClick(productID, this.props.productFullName);
  };
  render() {
    const { title, backGround, productID } = this.props;
    return (
      <button data-product-id={productID} className={`btn ${backGround}`} onClick={this.clickHeandler}>
        {title}
      </button>
    );
  }
}
