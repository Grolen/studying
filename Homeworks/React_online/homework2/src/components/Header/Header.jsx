import React, { Component } from 'react';
import StarSvg from '../../img/star.svg';
import './Header.scss';

export default class Header extends Component {
  render() {
    const { productsInCart, productsInFavorite } = this.props;
    return (
      <div className="header">
        <div className="header__logo">
        <h1>Google Store</h1>
        </div>
        <div className="header__items">
          <div className="item__cart">
        <img src="https://cdn-icons-png.flaticon.com/512/1413/1413925.png" width="30px"></img>
        <p>{`: ${productsInCart}`}</p>
          </div>
          <div className="item__favorite">
        <img src={StarSvg} width="30px"></img>
        <p>{`: ${productsInFavorite}`}</p>
        </div>
        </div>
      </div>
    );
  }
}
