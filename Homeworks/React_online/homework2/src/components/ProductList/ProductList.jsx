import React, { Component } from 'react';
import ProductCard from '../ProductCard/ProductCard';
import './ProductList.scss';

export default class ProductList extends Component {
  render() {
    const { loading, products, onClickBtn, favoriteProduct, unfavoriteProduct } = this.props;
    return (
      <div className="products">
        <div className="products__list">{loading ? <div>Loading...</div> : products.map((phone) => <ProductCard phone={phone} onClickBtn={onClickBtn} favorite={favoriteProduct} unfavorite={unfavoriteProduct} />)}</div>
      </div>
    );
  }
}
