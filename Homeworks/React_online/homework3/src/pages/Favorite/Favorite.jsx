import React from 'react'
import ProductCard from '../../components/ProductCard'
import Modal from '../../components/Modal'
import './Favorite.scss'

export default function Favorite({
  favorite,
  handleFavoriteProduct,
  generateModal,
  addItemToCart,
  modalShow,
  modalContent,
  closeModal,
}) {
  return (
    <>
      <div className="favorite">
        <div className="favorite__list">
          {favorite.length > 0 ? (
            favorite.map((phone) => (
              <ProductCard
                key={phone.articul}
                phone={phone}
                onClickBtn={generateModal}
                actionFnc={addItemToCart}
                modalText={`Add ${phone.name} (${phone.color}) to cart?`}
                isInCart={false}
                modalTitle={`Add to cart`}
                favoriteList={favorite}
                handleFavoriteProduct={handleFavoriteProduct}
              />
            ))
          ) : (
            <div className="favorite__list--empty">No products in favorite</div>
          )}
        </div>
      </div>
      {modalShow && <Modal modalContent={modalContent} onClose={closeModal} />}
    </>
  )
}
