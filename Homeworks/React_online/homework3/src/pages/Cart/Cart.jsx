import React from 'react'
import Modal from '../../components/Modal'
import ProductCard from '../../components/ProductCard'
import './Cart.scss'

export default function Cart({
  cart,
  generateModal,
  deleteItemFromCart,
  modalShow,
  modalContent,
  closeModal,
}) {
  return (
    <>
      <div className="cart">
        <div className="cart__list">
          {cart.length > 0 ? (
            cart.map((phone) => (
              <ProductCard
                key={phone.articul}
                phone={phone}
                onClickBtn={generateModal}
                actionFnc={deleteItemFromCart}
                modalText={`Delete ${phone.name} (${phone.color}) from cart?`}
                isInCart={true}
                modalTitle={`Delete from cart`}
              />
            ))
          ) : (
            <div className="cart__list--empty">No products in cart</div>
          )}
        </div>
      </div>
      {modalShow && <Modal modalContent={modalContent} onClose={closeModal} />}
    </>
  )
}
