import React from 'react'
import ProductList from '../../components/ProductList'
import Modal from '../../components/Modal'
import Header from '../../components/Header'

export default function Home({
  productsInCartCounter,
  productsInFavoriteCounter,
  loading,
  products,
  favorite,
  generateModal,
  addItemToCart,
  modalShow,
  modalContent,
  closeModal,
  handleFavoriteProduct,
}) {
  return (
    <>
      <Header
        productsInCart={productsInCartCounter}
        productsInFavorite={productsInFavoriteCounter}
      />
      <ProductList
        loading={loading}
        products={products}
        favorite={favorite}
        onClickBtn={generateModal}
        addItemToCart={addItemToCart}
        handleFavoriteProduct={handleFavoriteProduct}
      />
      {modalShow && <Modal modalContent={modalContent} onClose={closeModal} />}
    </>
  )
}
