import { Route, Routes } from 'react-router-dom'
import React from 'react'
import Cart from '../pages/Cart'
import Favorite from '../pages/Favorite'
import Error from '../pages/Error'
import Home from '../pages/Home'
import PropTypes from 'prop-types'

export default function AppRoutes({
  productsInCartCounter,
  productsInFavoriteCounter,
  loading,
  products,
  favorite,
  cart,
  generateModal,
  addItemToCart,
  deleteItemFromCart,
  modalShow,
  modalContent,
  closeModal,
  handleFavoriteProduct,
}) {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <Home
            productsInCartCounter={productsInCartCounter}
            productsInFavoriteCounter={productsInFavoriteCounter}
            loading={loading}
            products={products}
            favorite={favorite}
            generateModal={generateModal}
            addItemToCart={addItemToCart}
            modalShow={modalShow}
            modalContent={modalContent}
            closeModal={closeModal}
            handleFavoriteProduct={handleFavoriteProduct}
          />
        }
      ></Route>
      <Route
        path="/cart"
        element={
          <Cart
            cart={cart}
            generateModal={generateModal}
            deleteItemFromCart={deleteItemFromCart}
            modalShow={modalShow}
            modalContent={modalContent}
            closeModal={closeModal}
          />
        }
      ></Route>
      <Route
        path="/favorite"
        element={
          <Favorite
            favorite={favorite}
            handleFavoriteProduct={handleFavoriteProduct}
            generateModal={generateModal}
            addItemToCart={addItemToCart}
            modalShow={modalShow}
            modalContent={modalContent}
            closeModal={closeModal}
          />
        }
      ></Route>
      <Route path="*" element={<Error />} />
    </Routes>
  )
}

AppRoutes.defaultProps = {
  productsInCartCounter: 0,
  productsInFavoriteCounter: 0,
  loading: true,
  products: [],
  favorite: [],
  cart: [],
  generateModalAdd: () => {},
  generateModalCart: () => {},
  modalShow: false,
  modalContent: {},
  closeModal: () => {},
  handleFavoriteProduct: () => {},
}

AppRoutes.propTypes = {
  productsInCartCounter: PropTypes.number,
  productsInFavoriteCounter: PropTypes.number,
  products: PropTypes.array,
  favorite: PropTypes.array,
  cart: PropTypes.array,
  loading: PropTypes.bool,
  generateModalAdd: PropTypes.func,
  generateModalCart: PropTypes.func,
  modalContent: PropTypes.object,
  closeModal: PropTypes.func,
  modalShow: PropTypes.bool,
  handleFavoriteProduct: PropTypes.func,
}
