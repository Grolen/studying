import React from 'react'
import ProductCard from '../ProductCard/ProductCard'
import './ProductList.scss'

export default function ProductList({
  loading,
  products,
  favorite,
  onClickBtn,
  addItemToCart,
  handleFavoriteProduct,
}) {
  return (
    <div className="products">
      <div className="products__list">
        {loading ? (
          <div>Loading...</div>
        ) : (
          products.map((phone) => (
            <ProductCard
              key={phone.articul}
              phone={phone}
              onClickBtn={onClickBtn}
              actionFnc={addItemToCart}
              modalText={`Add ${phone.name} (${phone.color}) to cart?`}
              isInCart={false}
              modalTitle={`Add to cart`}
              favoriteList={favorite}
              handleFavoriteProduct={handleFavoriteProduct}
            />
          ))
        )}
      </div>
    </div>
  )
}
