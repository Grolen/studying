import React from 'react'
import './Modal.scss'

export default function Modal({ onClose, modalContent }) {
  const { header, closeButton, text, actions } = modalContent
  const closeHandler = () => {
    onClose()
  }

  return (
    <>
      <div className={`modal`}>
        <div className="modal__header">
          <h2 className="modal__header__title">{header}</h2>
          {closeButton && (
            <button className="modal__header__btn" onClick={closeHandler}>
              &times;
            </button>
          )}
        </div>
        <div className="modal__content">
          <p className="modal__content__text">{text}</p>
          {actions}
        </div>
      </div>
      <div className="backdrop" onClick={closeHandler} />
    </>
  )
}
