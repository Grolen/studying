import React from 'react'
import { Link } from 'react-router-dom'
import StarSvg from '../../img/star.svg'
import './Header.scss'

export default function Header({ productsInCart, productsInFavorite }) {
  return (
    <div className="header">
      <div className="header__logo">
        <Link to="/">
          <h1>Google Store</h1>
        </Link>
      </div>
      <div className="header__items">
        <Link to="/cart">Your Cart</Link>
        <div className="item__cart">
          <img
            src="https://cdn-icons-png.flaticon.com/512/1413/1413925.png"
            alt="cart"
            width="30px"
          ></img>
          <p>{`: ${productsInCart}`}</p>
        </div>
        <Link to="/favorite">Your Favorite</Link>
        <div className="item__favorite">
          <img src={StarSvg} width="30px" alt="star"></img>
          <p>{`: ${productsInFavorite}`}</p>
        </div>
      </div>
    </div>
  )
}
