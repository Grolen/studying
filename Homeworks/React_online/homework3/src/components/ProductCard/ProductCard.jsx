import React from 'react'
import Button from '../Button/Button'
import { ReactComponent as StarSvg } from '../../img/star.svg'

export default function ProductCard({
  phone,
  modalText,
  modalTitle,
  favoriteList,
  onClickBtn,
  actionFnc,
  handleFavoriteProduct,
  isInCart,
}) {
  const handleFavorite = () => {
    handleFavoriteProduct(phone)
  }

  const check = () => {
    const product = favoriteList.find(
      (product) => product.articul === phone.articul
    )
    return product ? true : false
  }

  return (
    <div id={phone.articul} className="item">
      <div className="item__image">
        <img src={phone.image} alt={phone.name} width="350px" height="350px" />
      </div>
      <div className="item__info">
        <h2>
          {phone.name} ({phone.color})
        </h2>
        {!isInCart ? (
          <span onClick={handleFavorite}>
            {check() ? <StarSvg fill="green" /> : <StarSvg />}
          </span>
        ) : null}

        <p>{phone.price}</p>
      </div>
      <div className="item__buttons">
        <Button
          productID={phone.articul}
          text={modalText}
          actionFnc={actionFnc}
          title={modalTitle}
          backGround="red"
          onClick={onClickBtn}
        />
      </div>
    </div>
  )
}
