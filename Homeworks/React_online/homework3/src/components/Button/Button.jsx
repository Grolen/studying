import React from 'react'
import './Button.scss'

export default function Button({
  title,
  backGround,
  productID,
  onClick,
  actionFnc,
  text,
}) {
  const clickHeandler = (e) => {
    const productID = e.target.dataset.productId
    onClick(productID, title, text, actionFnc)
  }
  return (
    <button
      data-product-id={productID}
      className={`btn ${backGround}`}
      onClick={clickHeandler}
    >
      {title}
    </button>
  )
}
