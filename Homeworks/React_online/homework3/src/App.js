import React, { useEffect, useState } from 'react'
import './App.scss'
import AppRoutes from './AppRoutes/AppRoutes'

const getHistoryFromLS = (itemArr) => {
  const lsHistory = localStorage.getItem(itemArr)
  if (!lsHistory) {
    return []
  }
  try {
    const value = JSON.parse(lsHistory)
    return value
  } catch (error) {
    return []
  }
}

function App() {
  const [products, setProducts] = useState([])
  const [loading, setLoading] = useState(true)
  const [favorite, setFavorite] = useState([])
  const [cart, setCart] = useState([])
  const [modalShow, setModalShow] = useState(false)
  const [modalContent, setModalContent] = useState({
    header: 'Som,ething went wrong',
    text: '',
    closeButton: true,
    actions: '',
  })

  useEffect(() => {
    ;(async function getProducts() {
      const response = await fetch('googlePhones.json')
      const products = await response.json()
      setProducts(products.phones)
      setLoading(false)
    })()
  }, [])

  useEffect(() => {
    const lsCart = getHistoryFromLS('cart')
    setCart(lsCart)
    const lsFavorite = getHistoryFromLS('favorite')
    setFavorite(lsFavorite)
  }, [])

  useEffect(() => {
    localStorage.setItem('favorite', JSON.stringify(favorite))
  }, [favorite])

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart))
  }, [cart])

  const showModal = () => {
    setModalShow(true)
  }

  const generateModal = (productID, header, text, actionFnc) => {
    showModal()
    setModalContent({
      ...modalContent,
      header: `${header}`,
      text: `${text}`,
      actions: (
        <>
          <button
            type="button"
            className="modal__content__btn"
            onClick={(event) => actionFnc(productID)}
          >
            Yes
          </button>
          <button
            type="button"
            className="modal__content__btn"
            onClick={closeModal}
          >
            No
          </button>
        </>
      ),
    })
  }

  const closeModal = () => {
    setModalShow(false)
  }

  const deleteItemFromCart = (productID) => {
    const newCart = cart.filter((item) => item.articul !== productID)
    setCart(newCart)
    closeModal()
  }

  const addItemToCart = (productId) => {
    closeModal()
    const product = products.find((product) => product.articul === productId)
    setCart([...cart, product])
  }

  //For Favorite

  const addFavoriteProduct = (phone) => {
    setFavorite([...favorite, phone])
  }

  const deleteFavoriteProduct = (phone) => {
    setFavorite(favorite.filter((product) => product.articul !== phone.articul))
  }

  const handleFavoriteProduct = (phone) => {
    const product = favorite.find((item) => item.articul === phone.articul)
    if (product) {
      deleteFavoriteProduct(phone)
    } else {
      addFavoriteProduct(phone)
    }
  }

  //For Header

  const productsInCartCounter = () => {
    const number = cart.length
    return number
  }

  const productsInFavoriteCounter = () => {
    const number = favorite.length
    return number
  }

  return (
    <>
      <AppRoutes
        productsInCartCounter={productsInCartCounter()}
        productsInFavoriteCounter={productsInFavoriteCounter()}
        products={products}
        favorite={favorite}
        cart={cart}
        loading={loading}
        generateModal={generateModal}
        addItemToCart={addItemToCart}
        deleteItemFromCart={deleteItemFromCart}
        modalContent={modalContent}
        closeModal={closeModal}
        modalShow={modalShow}
        handleFavoriteProduct={handleFavoriteProduct}
      />
    </>
  )
}

export default App
