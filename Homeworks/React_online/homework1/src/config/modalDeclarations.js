const modalWindowDeclarations = [
  {
    id: 'modalID1',
    header: 'Modal Window 1',
    closeButton: true,
    text: 'Is this a first modal window with a closeButton?',
    actions: (
      <>
        <button type="button" className="modal__content__btn" data-dismiss="modal">
          Yes
        </button>
        <button type="button" className="modal__content__btn">
          No
        </button>
      </>
    ),
  },
  {
    id: 'modalID2',
    header: 'Modal Window 2',
    closeButton: false,
    text: 'Is this a second modal window without a closeButton?',
    actions: (
      <>
        <button type="button" className="modal__content__btn" data-dismiss="modal">
          Yes
        </button>
        <button type="button" className="modal__content__btn">
          No
        </button>
      </>
    ),
  },
];
export default modalWindowDeclarations;
