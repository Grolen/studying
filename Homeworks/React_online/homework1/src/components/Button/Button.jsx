import React, { Component } from 'react';
import modalWindowDeclarations from '../../config/modalDeclarations';
import './Button.scss';

export default class Button extends Component {
  clickHeandler = (e) => {
    const modalID = e.target.dataset.modalId;
    const modalInfoObject = modalWindowDeclarations.find((modal) => modal.id === modalID);
    this.props.onClick(modalInfoObject);
  };
  render() {
    const { title, backGround, modalID } = this.props;
    return (
      <button data-modal-id={modalID} className={`btn ${backGround}`} onClick={this.clickHeandler}>
        {title}
      </button>
    );
  }
}
