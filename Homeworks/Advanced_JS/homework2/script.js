'use strict';

const books = [
  {
    author: 'Скотт Бэккер',
    name: 'Тьма, что приходит прежде',
    price: 70,
  },
  {
    author: 'Скотт Бэккер',
    name: 'Воин-пророк',
  },
  {
    name: 'Тысячекратная мысль',
    price: 70,
  },
  {
    author: 'Скотт Бэккер',
    name: 'Нечестивый Консульт',
    price: 70,
  },
  {
    author: 'Дарья Донцова',
    name: 'Детектив на диете',
    price: 40,
  },
  {
    author: 'Дарья Донцова',
    name: 'Дед Снегур и Морозочка',
  },
];

class Homework2 {
  constructor(books, arrOfProps) {
    this.books = books;
    this.arrOfProps = arrOfProps;
    this.elements = this.createDOM();
    this.showResult();
  }

  createDOM() {
    const root = document.createElement('div');
    root.classList.add('root');
    document.body.appendChild(root);
    const ul = document.createElement('ul');
    root.appendChild(ul);
    return { root, ul };
  }

  showResult() {
    for (const book of books) {
      try {
        for (const key of this.arrOfProps) {
          if (book[key] === undefined) {
            throw new Error(`В книге ${book.name} отсутствует поле ${key}`);
          }
        }
        const li = document.createElement('li');
        li.innerHTML = Object.entries(book)
          .map(([key, value]) => `${key}: ${value}`)
          .join(', ');
        this.elements.ul.appendChild(li);
      } catch (e) {
        console.log(e);
      }
    }
  }
}

const homework2 = new Homework2(books, ['author', 'name', 'price']);
