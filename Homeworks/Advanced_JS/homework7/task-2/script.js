/**
 * Написати клас Modal, за допомогою якого створити 2 об'єкта
 * модальних вікон: loginModal та signUpModal
 *
 * * loginModal має наступні параметри:
 * id - 'login-modal'
 * text - 'Ви успішно увійшли'
 * classList - 'modal login-modal'
 *
 * signUpModal має наступні параметри:
 * id - 'sign-up-modal'
 * text - 'Реєстрація'
 * classList - 'modal sign-up-modal'
 *
 * Кожне модальне вікно обов'язково має наступні методи:
 * - render() - генерує html код модального вікна
 * - open() - показує модальне вікно
 * - close() - закриває модальне вікно
 *
 * - За допомогою методу render() додати html код
 * модальних вікок в кінець body
 * - При натисканні на кнопку Login за допомогою методу openModal
 * відкривати модальне вікно loginModal
 * - При натисканні на кнопку Sign Up за допомогою методу openModal
 * відкривати модальне вікно signUpModal
 *
 */

class Modal {
  constructor(id, text, classList) {
    this.id = id;
    this.text = text;
    this.classList = classList;
    const modal = document.createElement('div');
    this.modal = modal;
    const modalContent = document.createElement('div');
    this.modalContent = modalContent;
    const closeSpan = document.createElement('span');
    this.closeSpan = closeSpan;
  }

  open() {
    this.modal.classList.add('active');
    this.closeSpan.addEventListener('click', () => {
      this.close();
    });
  }

  close() {
    this.modal.classList.remove('active');
  }

  render() {
    this.modal.className = this.classList;
    document.body.append(this.modal);
    this.modalContent.className = 'modal-content';
    this.modalContent.id = this.id;
    this.modalContent.innerHTML = this.text;
    this.modal.append(this.modalContent);
    this.closeSpan.className = 'close';
    this.closeSpan.innerHTML = 'x';
    this.modalContent.append(this.closeSpan);
  }
}

const loginModal = new Modal('login-modal', 'Ви успішно увійшли', 'modal login-modal');
loginModal.render();

const loginButton = document.querySelector('#login-btn');
loginButton.addEventListener('click', () => {
  loginModal.open();
});

const signUpModal = new Modal('sign-up-modal', 'Реєстрація', 'modal sign-up-modal');
signUpModal.render();

const signUpButton = document.querySelector('#sign-up-btn');
signUpButton.addEventListener('click', () => {
  signUpModal.open();
});
