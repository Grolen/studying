'use strict';

class SquareGame {
  constructor() {
    this.createTable10x10();
    this.createGame();
  }

  createGame() {
    this.isGameStarted = false;
    this.square = document.querySelectorAll('td');
    this.shuffleSquares();
    this.menu = document.createElement('div');
    this.menu.classList.add('menu');
    this.menuText = document.createElement('p');
    this.menuText.innerText = 'Click to start';
    this.newGameButton = document.createElement('button');
    this.newGameButton.classList.add('new-game');
    this.newGameButton.innerText = 'New Game';
    this.newGameButton.addEventListener('click', () => {
      if (!this.isGameStarted) {
        this.startGame();
        this.menuText.innerText = 'Game started';
      }
    });
    this.stopGameButton = document.createElement('button');
    this.stopGameButton.classList.add('stop-game');
    this.stopGameButton.innerText = 'Stop Game';
    this.stopGameButton.addEventListener('click', () => {
      if (this.isGameStarted) {
        this.stopGame();
      }
    });

    this.changeLevelText = document.createElement('p');
    this.changeLevelText.innerText = 'Choose level:';
    this.changeLevelSelect = document.createElement('select');
    this.changeLevelSelect.classList.add('change-level');
    this.easyLevel = document.createElement('option');
    this.easyLevel.innerText = 'Easy';
    this.easyLevel.value = 'easy';
    this.mediumLevel = document.createElement('option');
    this.mediumLevel.innerText = 'Medium';
    this.mediumLevel.value = 'medium';
    this.hardLevel = document.createElement('option');
    this.hardLevel.innerText = 'Hard';
    this.hardLevel.value = 'hard';
    this.changeLevelSelect.appendChild(this.easyLevel);
    this.changeLevelSelect.appendChild(this.mediumLevel);
    this.changeLevelSelect.appendChild(this.hardLevel);
    this.menu.appendChild(this.menuText);
    this.menu.appendChild(this.newGameButton);
    this.menu.appendChild(this.stopGameButton);
    this.menu.appendChild(this.changeLevelText);
    this.menu.appendChild(this.changeLevelSelect);
    document.body.appendChild(this.menu);
    this.createScoreBoard();
  }

  createScoreBoard() {
    this.scoreBoard = document.createElement('div');
    this.scoreBoard.classList.add('score-board');
    this.user = document.createElement('div');
    this.user.classList.add('user-points');
    this.user.innerText = `User: 0`;
    this.userPoints = 0;
    this.pc = document.createElement('div');
    this.pc.classList.add('pc-points');
    this.pc.innerText = `PC: 0`;
    this.pcPoints = 0;
    this.menu.appendChild(this.scoreBoard);
    this.scoreBoard.appendChild(this.user);
    this.scoreBoard.appendChild(this.pc);
  }

  shuffleSquares() {
    this.shuffledSquares = Array.from(this.square).sort(() => Math.random() - 0.5);
  }

  changeLevel() {
    this.miliseconds = 0;
    if (this.changeLevelSelect.value === 'easy') {
      this.miliseconds = 1500;
    } else if (this.changeLevelSelect.value === 'medium') {
      this.miliseconds = 1000;
    } else if (this.changeLevelSelect.value === 'hard') {
      this.miliseconds = 500;
    }
  }

  startGame() {
    this.shuffleSquares();
    this.changeLevel();
    this.isGameOver = false;
    this.isGameStarted = true;
    this.i = 0;
    this.getRandomBlueSquare();
  }

  createTable10x10() {
    this.table = document.createElement('table');
    this.table.classList.add('table');
    for (let i = 0; i < 10; i++) {
      this.tr = document.createElement('tr');
      for (let j = 0; j < 10; j++) {
        this.td = document.createElement('td');
        this.tr.appendChild(this.td);
      }
      this.table.appendChild(this.tr);
    }
    document.body.appendChild(this.table);
  }

  getRandomBlueSquare() {
    this.gameOver();
    if (!this.isGameOver) {
      this.getUserClick();
      setTimeout(() => {
        this.shuffledSquares[this.i].style.backgroundColor = 'blue';
        this.getRandomRedSquare();
      }, 0);
    }
  }
  getRandomRedSquare() {
    setTimeout(() => {
      if (this.shuffledSquares[this.i].style.backgroundColor === 'blue') {
        this.shuffledSquares[this.i].style.backgroundColor = 'red';
        this.getPcPoint();
      }
      this.i++;
      this.getRandomBlueSquare();
    }, this.miliseconds);
  }
  getUserClick() {
    this.square.forEach((item) => {
      item.addEventListener('click', () => {
        if (item.style.backgroundColor === 'blue') {
          item.style.backgroundColor = 'green';
          this.getUserPoint();
        }
      });
    });
  }

  getPcPoint() {
    this.pcPoints++;
    this.pc.innerText = `PC: ${this.pcPoints}`;
  }

  getUserPoint() {
    this.userPoints++;
    this.user.innerText = `User: ${this.userPoints}`;
  }

  stopGame() {
    this.isGameOver = true;
    this.isGameStarted = false;
    this.menuText.innerText = 'Game over';
    this.newGameButton.innerText = 'Restart Game';
    this.userPoints = 0;
    this.pcPoints = 0;
    this.user.innerText = `User: 0`;
    this.pc.innerText = `PC: 0`;
    this.shuffledSquares.forEach((item) => {
      item.style.backgroundColor = 'white';
    });
  }

  gameOver() {
    if (this.userPoints === 50) {
      this.isGameOver = true;
      this.menuText.innerText = 'You win!';
    } else if (this.pcPoints === 50) {
      this.isGameOver = true;
      this.menuText.innerText = 'You lose!';
    }
  }
}

const result = new SquareGame();
