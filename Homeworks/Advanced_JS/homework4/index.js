async function fetchData(url) {
  const data = await fetch(url);
  const jsonData = await data.json();
  return jsonData;
}

function createElement(tag, className, text) {
  const element = document.createElement(tag);
  element.className = className;
  element.innerText = text;
  return element;
}

function renderRootElements() {
  const container = createElement('div', 'container', '');
  document.body.appendChild(container);
  const filmList = createElement('ul', 'film-list', '');
  container.appendChild(filmList);
}

async function renderData() {
  const data = await fetchData('https://ajax.test-danit.com/api/swapi/films');
  const filmList = document.querySelector('.film-list');
  data.forEach(film => {
    const filmContainer = createElement('li', 'film-container', `Name: ${film.name}, Episode: ${film.episodeId}`);
    const openingCrawl = createElement('p', 'opening-crawl', film.openingCrawl);
    renderCharacters(film, filmContainer);
    filmContainer.appendChild(openingCrawl);
    filmList.appendChild(filmContainer);
  });
}

async function renderCharacters(film, filmContainer) {
  const charactersList = createElement('ul', 'characters-list', '');
  const charactersArray = film.characters;
  const processedCharactersArray = charactersArray.map(async character => {
    const characterData = await fetchData(character);
    return characterData;
  });
  const processedCharacters = await Promise.all(processedCharactersArray);
  processedCharacters.forEach(character => {
    const characterName = createElement('li', 'character-name', character.name);
    charactersList.appendChild(characterName);
  });
  filmContainer.appendChild(charactersList);
}

function init() {
  renderRootElements();
  renderData();
}

init();