'use strict';

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(newName) {
    this._name = value;
  }

  get age() {
    return this._age;
  }
  set age(newAge) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get lang() {
    return this._lang;
  }
  set lang(newLang) {
    this._lang = value;
  }
  get salary() {
    return this._salary * 3;
  }
}

const testEmployee = new Employee('Иван', 25, 1000);
const testProgrammer = new Programmer('Иван', 25, 1000, 'JavaScript');
console.log(testEmployee);
console.log(testProgrammer);
