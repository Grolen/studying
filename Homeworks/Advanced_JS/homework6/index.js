const root = document.querySelector('.root');

function createElement(name = 'div', className = 'default-div', text = 'Default div', event = '') {
   const element = document.createElement(name);
    element.className = className;
   element.innerHTML = text;
    if (event) {
      element.addEventListener('click', async () => {
    event()
   })
  }
   root.appendChild(element)
}

async function getIP() {
  const ip = await fetch("https://api.ipify.org?format=json");
  const ipJson = await ip.json();
  return ipJson;
}

async function onClickButton() {
  const info = document.querySelectorAll('.ip-info');
  if (info.length > 0) {
    info.forEach(element => {
      element.remove();
    });
  }
  const ip = await getIP();
  createElement('div', 'ip-info', `Your IP is: ${ip.ip}`)
  getDataFromServer(ip.ip)
}


async function getDataFromServer(ip) {
 const data = await fetch(`http://ip-api.com/json/${ip}`);
 const dataJson = await data.json();
 showInfo(dataJson)
}

function showInfo(data) {
  createElement('div', 'ip-info', `Your country is: ${data.country} <br> 
  Your region is: ${data.regionName} <br>
  Your city is: ${data.city} <br>
  Your timezone is: ${data.timezone}`)
}


function init() {
  createElement('button', 'ip', 'Знайти по IP', onClickButton)
}


init();


