let userName = prompt('What is your name?');
let userAge = prompt('How old are you?');

while (userName === '' || isNaN(userAge)) {
  userName = prompt('What is your name?', userName);
  userAge = prompt('How old are you?', userAge);
  if (userName === false || userAge === false) {
    break;
  }
}

if (userAge < 18) {
  alert('You are not allowed to visit this website');
} else if (userAge <= 22) {
  if (confirm('Are you sure you want to continue?')) {
    alert('Welcome ' + userName);
  } else {
    alert('You are not allowed to visit this website');
  }
} else {
  alert('Welcome ' + userName);
}
