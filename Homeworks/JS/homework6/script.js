function filterBy(array, dataType) {
  if (Array.isArray(array)) {
    const newArray = array.filter((element) => typeof element !== dataType);
    console.log(newArray);
  } else {
    alert("Error");
  }
}

const arr = [
  "string with space",
  "string",
  26,
  56,
  null,
  undefined,
  null,
  function () {},
  [],
];
const filter = prompt(
  "Enter data type: undefined, number, bigint, boolean, string, symbol, object, function"
);
filterBy(arr, filter);
