// function createNewUser() {
//   this.firstName = prompt("Enter your firstname");
//   this.lastName = prompt("Ender your lastname");
//   this.getLogin = () => {
//     return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
//   };
//   this.birthday = prompt("Enter your birthday (dd.mm.yyyy)");
//   let splitBirthday = this.birthday.split(".");
//   let now = new Date();
//   let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
//   let dob = new Date(splitBirthday[2], splitBirthday[1], splitBirthday[0]);
//   let dobNow = new Date(
//     today.getFullYear(),
//     splitBirthday[1],
//     splitBirthday[0]
//   );

//   this.getAge = () => {
//     if (today < dobNow) {
//       return today.getFullYear() - dob.getFullYear() - 1;
//     } else {
//       return today.getFullYear() - dob.getFullYear();
//     }
//   };
//   this.getPassword = () => {
//     return (
//       this.firstName.charAt(0) + this.lastName.toLowerCase() + splitBirthday[2]
//     );
//   };
// }
// let newUser = new createNewUser();
// console.log(
//   `Your login ${newUser.getLogin()}, your password is ${newUser.getPassword()} you ${newUser.getAge()} y.o. and`
// );

function createNewUser() {
  this.firstName = prompt("Введите ваше имя");
  this.lastName = prompt("Введите вашу фамилию");
  this.birthday = prompt(
    "Введите дату Вашего рождения, текст в формате dd.mm.yyyy"
  );

  this.getLogin = function () {
    return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
  };

  this.getAge = function () {
    const today = new Date();
    const userBirthday = Date.parse(
      `${this.birthday.slice(6)}-${this.birthday.slice(
        3,
        5
      )}-${this.birthday.slice(0, 2)}`
    );
    const age = (
      (today - userBirthday) /
      (1000 * 60 * 60 * 24 * 30 * 12)
    ).toFixed(0);

    if (age < today) {
      return `Вам ${age - 1} лет`;
    } else {
      return `Вам ${age} лет`;
    }
  };

  this.getPassword = function () {
    return `${this.firstName[0].toUpperCase()}${this.lastName.toLocaleLowerCase()}${this.birthday.slice(
      -4
    )}`;
  };
}

let newUser = new createNewUser();
console.log("Ваш логин: " + newUser.getLogin());
console.log("Ваш возраст: " + newUser.getAge());
console.log("Ваш пароль: " + newUser.getPassword());
