// Добавил вторую версию т.к. перечитывал описание и немного запутался кто что должен создавать, а кто что должен вызывать, может быть этот вариант более правильный

function createNewUser() {
  this.firstName = prompt("Enter ur firstname");
  this.lastName = prompt("Ender ur lastname");
  this.getLogin = function () {
    return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
  };
}

let newUser = new createNewUser();

console.log("Ur login " + newUser.getLogin());

// Добавил третью версию, где переписал данный конструктор на ES6 синтаксис. Но она, как и вторая, в createNewUser не создаёт объект newUser, он создаётся вручную. Технически это правильный тоже вариант решения задачи?

// class createNewUser {
//   constructor(
//     firstName = prompt("Enter ur firstname"),
//     lastName = prompt("Ender ur lastname")
//   ) {
//     this.firstName = firstName;
//     this.lastName = lastName;
//   }

//   getLogin() {
//     return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
//   }
// }

// let newUser = new createNewUser();
// console.log(newUser.getLogin());
