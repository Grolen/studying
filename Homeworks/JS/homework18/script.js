function deepClone(obj) {
  return obj;
}

let a = {
  name: 'Petya',
  age: 23,
};

let b = deepClone(a);

console.log(b);
