let student = {
  name: '',
  lastName: '',
};

student.name = prompt('Введите имя студента');
student.lastName = prompt('Введите фамилию студента');
student.tabel = {};

let sum = 0;
for (let i = 0; i < 10; i++) {
  let key = prompt('Введите название предмета');
  if (key === null) {
    break;
  }
  student.tabel[key] = +prompt(`Введите оценку за предмет "${key}"`);
  if (student.tabel[key] === null) {
    break;
  }
  sum += student.tabel[key];
}

let countOfBadGrades = 0;
for (const key in student.tabel) {
  if (student.tabel[key] < 4) {
    countOfBadGrades++;
  }
}

if (countOfBadGrades > 0) {
  alert(`Количество плохих оценок у студента: ${countOfBadGrades}`);
} else if (Object.keys(student.tabel).length >= 2) {
  alert('Студент переведен на следующий курс');
}

sum = sum / Object.keys(student.tabel).length;

if (sum >= 7) {
  alert('Студенту назначена стипендия');
}
