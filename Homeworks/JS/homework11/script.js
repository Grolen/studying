const allKeys = document.querySelectorAll(".btn");

// function selectKey(event) {
//   const key = event.key;
//   for (const button of allKeys) {
//     button.classList.remove("active");
//   }
// }
function selectKey() {
  window.addEventListener("keydown", (event) => {
    const key = event.key;
    for (const btn of allKeys) {
      btn.classList.remove("active");
      if (btn.innerText === key) {
        btn.classList.add("active");
      }
    }
  });
}

selectKey();
