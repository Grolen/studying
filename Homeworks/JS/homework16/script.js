let userNum = '';

const checkIfNumber = (num) => Number.isNaN(Number.parseFloat(num));

do {
  userNum = prompt('Enter your number', `${userNum}`);
  if (userNum === null) {
    break;
  }
} while (checkIfNumber(userNum));

f = (n) => (n < 2 ? (n < 0 ? f(n + 2) - f(n + 1) : n) : f(n - 1) + f(n - 2));

alert(`${f(userNum)}`);
