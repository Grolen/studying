let userNum = '';

const checkIfNumber = (num) => Number.isNaN(Number.parseFloat(num));

do {
  userNum = prompt('Enter your number', `${userNum}`);
  if (userNum === null) {
    break;
  }
} while (checkIfNumber(userNum));

function factorial(n) {
  return n === 1 ? n : n * factorial(n - 1);
}

alert(`${factorial(userNum)}`);
