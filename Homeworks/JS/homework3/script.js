let a;
let b;
let operation;

function isValidNum(num) {
  return Number.isNaN(Number.parseFloat(num)) || num === null;
}

function isValidOp(op) {
  return op === "+" || op === "-" || op === "*" || op === "/";
}

function isValidDivision(divider, operat) {
  if (Number(divider) === 0 && operat === "/") {
    alert("На ноль делить нельзя. Введете все заново.");
    return Number(divider) === 0 && operat === "/";
  }
}

do {
  a = prompt("Введите первое число", a);
  b = prompt("Введите второе число", b);
  operation = prompt(
    "Введите математическую операцию. Допустимые значения: +, -, *, /"
  );
} while (
  isValidNum(a) ||
  isValidNum(b) ||
  !isValidOp(operation) ||
  isValidDivision(b, operation)
);

function calculator(arg1, arg2, whatToDo) {
  switch (whatToDo) {
    case "+":
      return Number(arg1) + Number(arg2);
    case "-":
      return Number(arg1) - Number(arg2);
    case "*":
      return Number(arg1) * Number(arg2);
    case "/":
      return Number(arg1) / Number(arg2);
  }
}

console.log(`${a} ${operation} ${b} = ${calculator(a, b, operation)}`);