let userNum;

while (true) {
  userNum = +prompt("Enter the number", "");
  if (Number.isInteger(userNum) && userNum) break;
}

if (userNum < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 5; i <= userNum; i += 5) {
    console.log(i);
  }
}

function primeNums() {
  let m;
  let n;

  while (true) {
    m = +prompt("Enter your first number", "");
    n = +prompt("Enter your second number", "");
    if (m < n) {
      break;
    } else {
      alert(m + " and " + n + " are wrong numbers!");
    }
  }

  for (let i = m; i <= n; i++) {
    let isPrime = true;
    for (let j = 2; j < i; j++) {
      if (i % j === 0) {
        isPrime = false;
        break;
      }
    }
    if (isPrime) console.log(i);
  }
}

primeNums();
