const tabMenu = document.querySelector('.tabs');
console.log(tabMenu);
tabMenu.addEventListener('click', (event) => {
  if (event.target.tagName === 'LI') {
    change(event);
  }
});
const change = (event) => {
  let menuItem = document.querySelector('.tabs-title.active');
  menuItem.classList.remove('active');
  event.target.classList.add('active');
  let id = event.target.innerHTML.toLowerCase();
  let contentItem = document.querySelector('.content-item.active');
  contentItem.classList.remove('active');
  document.getElementById(id).classList.add('active');
};
