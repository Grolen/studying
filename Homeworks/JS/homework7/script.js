function list(arr, parent = "body") {
  const list = document.createElement("ul");
  document.querySelector(parent).append(list);
  let resultArr = arr.map((elem) => {
    return `<li>${elem}<li>`;
  });
}
list(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);

//версия без пробелов

// const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

// function addList(arr, parent = "body") {
//   document.querySelector(parent).innerHTML = `<ul>${arr.map(
//     (item) => `<li>${item}</li>`
//   )}</ul>`;
// }
// addList(arr);
